import java.util.Scanner;

public class Homework05 {
    public static void main(String[] args) {
        //Создаю объект Scanner
        Scanner scanner = new Scanner(System.in);
        //считываю ввод с клавиатуры
        int namber = scanner.nextInt();

        int digit = 0;
        int minDigit = 9;
        while (namber != -1) {
            while (namber != 0) {
                int lastDigit = namber % 10;
                namber = namber / 10;
                digit = lastDigit;
                if (digit < minDigit) {
                    minDigit = digit;
                }
            }
            namber = scanner.nextInt();
        }
        System.out.println("resalt -" + minDigit);
    }

}

